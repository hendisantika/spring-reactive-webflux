# spring-reactive-webflux
Spring reactive webflux example with server and client part. 

#### Requirements
For building and running the application you need:

1. JDK 1.8
2. Maven 3

Running the application locally
You can use the Spring Boot Maven plugin like so:
```
mvn spring-boot:run
```