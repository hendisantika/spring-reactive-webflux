package com.hendisantika.springreactivewebflux;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;

@SpringBootApplication
public class SpringReactiveWebfluxApplication {
    Logger logger = LoggerFactory.getLogger(SpringReactiveWebfluxApplication.class);

    public static void main(String[] args) {
//        SpringApplication.run(SpringReactiveWebfluxApplication.class, args);
        new SpringApplicationBuilder(SpringReactiveWebfluxApplication.class).properties(java.util.Collections.singletonMap("server.port", "8080"))
                .run(args);
    }

    @Bean
    WebClient getWebClient() {
        return WebClient.create("http://localhost:8080");
    }

    @Bean
    CommandLineRunner demo(WebClient client) {
        return args -> {
            client.get()
                    .uri("/temperatures")
                    .accept(MediaType.TEXT_EVENT_STREAM)
                    .retrieve()
                    .bodyToFlux(Integer.class)
                    .map(s -> String.valueOf(s))
                    .subscribe(msg -> {
                        logger.info(msg);
                    });
        };
    }

}

